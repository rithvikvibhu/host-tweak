Const ForReading = 1
Const ForWriting = 2

Set objFSO = CreateObject("Scripting.FileSystemObject")
Set objFile = objFSO.OpenTextFile("c:\windows\system32\drivers\etc\hosts", ForReading)

strText = objFile.ReadAll
objFile.Close
strNewText = Replace(strText, "74.125.227.67", "192.168.1.15")

Set objFile = objFSO.OpenTextFile("c:\windows\system32\drivers\etc\hosts", ForWriting)
objFile.WriteLine strNewText
objFile.Close