@ECHO OFF

REM Clear screen at startup
CLS

REM App Info
ECHO ------------------- Host Tweak ------------------------
ECHO This application is written in batch and vbscript to modify
ECHO the hosts file in windows as I wish.
ECHO Version: meh.
ECHO Build date: meh.
ECHO.
ECHO (c) 2015 Rithvik Vibhu. All Rights Reserved.
ECHO Published under GNU GENERAL PUBLIC LICENSE (GPL) v3.0
ECHO -------------------------------------------------------
ECHO.
ECHO.
ECHO.

REM Main Menu
:MENU
ECHO ------------- MAIN MENU -------------
ECHO -------------------------------------
ECHO 1.  First Time (Add entries)
ECHO 2.  Temporary (Switch IPs)
ECHO 3.  Last Time (Remove entries)
ECHO -------------------------------------
ECHO --------- PRESS 'Q' TO QUIT ---------
ECHO.

SET INPUT=
SET /P INPUT=Please select an option:

IF /I '%INPUT%'=='1' GOTO addEntries
IF /I '%INPUT%'=='2' GOTO runTempChooser               
IF /I '%INPUT%'=='3' GOTO removeEntries
IF /I '%INPUT%'=='Q' GOTO Quit


ECHO ----------- INVALID INPUT -----------
ECHO -------------------------------------
ECHO Please select a number from the Main
echo Menu [1-3] or select 'Q' to quit.
ECHO -------------------------------------
ECHO ----- PRESS ANY KEY TO CONTINUE -----

PAUSE > NUL
CLS
GOTO MENU

:addEntries
REM Clear screen and Add entries to hosts file
CLS
ECHO # START HACK									# f0>>%WINDIR%\system32\drivers\etc\hosts
ECHO ^192.168.1.7				google.com			# f0>>%WINDIR%\system32\drivers\etc\hosts
ECHO ^192.168.1.7				www.google.com		# f0>>%WINDIR%\system32\drivers\etc\hosts
ECHO ^192.168.1.7				google.co.in		# f0>>%WINDIR%\system32\drivers\etc\hosts
ECHO ^192.168.1.7				www.google.co.in	# f0>>%WINDIR%\system32\drivers\etc\hosts
ECHO ^74.125.227.7				fakegoogle.org		# f0>>%WINDIR%\system32\drivers\etc\hosts
ECHO ^74.125.227.7				www.fakegoogle.org	# f0>>%WINDIR%\system32\drivers\etc\hosts
ECHO # END HACK										# f0>>%WINDIR%\system32\drivers\etc\hosts

ECHO Done!

GOTO Quit

:runTempChooser
switch.bat
GOTO MENU
EXIT

:removeEntries
REM Clear screen and run remove.vbs to remove entries from hosts file
wscript.exe remove.vbs
ECHO Done!
                                                 
:Quit
EXIT