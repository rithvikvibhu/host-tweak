# Host Tweak #

This application is written in batch and vbscript to modify the hosts file in windows as i wish.

### What this does ###

* Add hosts entries for redirecting Google domains to another preferred IP
* Remove entries and revert to default
* Temporarily switch IPs without removing entries

### Installation ###

Its ready to use and portable!

### Usage ###

Run default.bat and follow instructions.