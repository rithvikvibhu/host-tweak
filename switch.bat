@ECHO OFF
CLS

:MENU
REM Menu
CLS
ECHO ------------ TEMP MENU --------------
ECHO -------------------------------------
ECHO 1.  Off (Real)
ECHO 2.  On (Fake)
ECHO -------------------------------------
ECHO --------- PRESS 'Q' TO QUIT ---------
ECHO.

SET INPUT=
SET /P INPUT=Please select a number:

IF /I '%INPUT%'=='1' GOTO off
IF /I '%INPUT%'=='2' GOTO on
IF /I '%INPUT%'=='Q' GOTO Quit

ECHO ----------- INVALID INPUT -----------
ECHO -------------------------------------
ECHO Please select a number from the Main
echo Menu [1-2] or select 'Q' to quit.
ECHO -------------------------------------
ECHO ----- PRESS ANY KEY TO CONTINUE -----

PAUSE > NUL
GOTO MENU

:off
REM Make it look removed
wscript.exe toggleOff.vbs
ECHO .
ECHO .
ECHO .
ECHO .
ECHO Done!
PAUSE
GOTO Quit


:on
REM Get it back
wscript.exe toggleOn.vbs
ECHO .
ECHO .
ECHO .
ECHO .
ECHO Done!
PAUSE
GOTO Quit

:Quit
default.bat
EXIT